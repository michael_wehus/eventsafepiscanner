var ws281x = require('rpi-ws281x');
const config = require('./config.json');

class lightEffects {

    constructor() {
        this.config = {};
        // Number of leds in my strip
        this.config.leds = 24;
        // Use DMA 10 (default 10)
        this.config.dma = 10;
        // Set full brightness, a value from 0 to 255 (default 255)
        this.config.brightness = 255;
        // Set the GPIO number to communicate with the Neopixel strip (default 18)
        this.config.gpio = 18;
        // The RGB sequence may vary on some strips. Valid values
        // are "rgb", "rbg", "grb", "gbr", "bgr", "brg".
        // Default is "rgb".
        // RGBW strips are not currently supported.
        this.config.strip = 'grb';
        // Configure ws281x
        ws281x.configure(this.config);
    }
    backlight() {
        allLeds(config.lighting.backlight.red, config.lighting.backlight.green, config.lighting.backlight.blue);
    }
    blink(leds) {
        allLeds(leds.red, leds.green, leds.blue);
        setTimeout(allLeds(leds.red, leds.green, leds.blue), (leds.timeout / 3));
        setTimeout(allLeds(0, 0, 0), (leds.timeout / 3));
        setTimeout(allLeds(leds.red, leds.green, leds.blue), (leds.timeout / 3) * 2);
        this.backlight();
    }
    ledByLed(red,green,blue,i,callback=false) {
        var pixels = new Uint32Array(this.config.leds);
        var color = (red << 16) | (green << 8) | blue;
        pixels[i] = color;
        ws281x.render(pixels);
        i++;
        if (i < this.config.leds) {
            setTimeout(this.ledByLed(red, green, blue, i), 50);
        } else if (typeof (callback)=="function") {
            callback();
        }
    }
    allLeds(red, green, blue) {
        // Create a pixel array matching the number of leds.
        // This must be an instance of Uint32Array.
        var pixels = new Uint32Array(this.config.leds);

        // Create a fill color with red/green/blue.
        
        var color = (red << 16) | (green << 8) | blue;

        for (var i = 0; i < this.config.leds; i++)
            pixels[i] = color;

        // Render to strip
        ws281x.render(pixels);
    }

};
module.exports = lightEffects;