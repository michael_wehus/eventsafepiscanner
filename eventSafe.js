'use strict';
const https = require('https');
class eventSafe {
    constructor(settings) {
        this.key = settings.apiKey;
        this.endpoint = settings.endpoint;
        this.heartBeat();
        setInterval(this.heartBeat.bind(this), 60000);
    }
    //Register scanner to event (triggered by scanning barcode on event page)
    eventRegistration(eventId, userId) {
       https.get(this.endpoint + '/regScanner/' + this.key + '/' + eventId + '/' + userId, (res) => {
       console.log('statusCode:', res.statusCode);
            console.log('headers:', res.headers);

            res.on('data', (d) => {
                process.stdout.write(d);
            });

        }).on('error', (e) => {
            console.error(e);
        });
    }
    //Send hearthBeat to eventSafe used for status monitoring 
    heartBeat() {
        
        https.get(this.endpoint + '/ImHere/' + this.key + '/1/Unknown', (res) => {
            console.log('ImHere statusCode: ' + this.key + ':', res.statusCode);
            res.on('data', (d) => {
                process.stdout.write(d);
            });
        }).on('error', (e) => {
            console.error(e);
        });
    }
    //Sjekker id p� b�nd/badge mot eventSafe (Success og failed callback functions)
    checkId(id, success, fail) {
        https.get(this.endpoint + '/InOut/' + this.key + '/' + id, (res) => {
            var body = '';
            console.log('ScanBand: ' + id + ':', res.statusCode);
            res.on('data', function (chunk) {
                body += chunk;
            });
            res.on('end', function () {
                try {
                    success(JSON.parse(body)[0]);
                } catch (e) {
                    if (e instanceof SyntaxError) {
                        fail();
                    } else {
                        console.error(e);
                    }
                }
            });
        }).on('error', (e) => {
            console.error(e);
        });
    }
}
module.exports = eventSafe;